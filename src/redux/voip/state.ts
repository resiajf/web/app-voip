import { CallRecord } from 'src/common/lib/api/voip/interfaces';

export interface VoIPState {
    loading: boolean;
    list: CallRecord[][];
    totalPages?: number;
    page?: number;
}