import { Action, Dispatch } from 'redux';

import { CallRecord } from 'src/common/lib/api/voip/interfaces';
import { GetListParameters, VoIP } from 'src/common/lib/api/voip/methods';
import { catchErrors as f } from 'src/common/redux/errors/decorator';

export const LOADING = 'voip:LOADING';
export const CLEAR_LOADING = 'voip:CLEAR_LOADING';
export const LOADED_PAGE = 'voip:LOADED_PAGE';
export const CLEAR_PAGE = 'voip:CLEAR_PAGE';

interface LoadingAction extends Action<typeof LOADING> {
    page: number;
    clearList?: boolean;
}

type ClearLoadingAction = Action<typeof CLEAR_LOADING>;

interface LoadedPageAction extends Action<typeof LOADED_PAGE> {
    values: CallRecord[];
    page: number;
    pages: number;
}

type ClearPageAction = Action<typeof CLEAR_PAGE>;

export type Actions = LoadingAction | ClearLoadingAction | LoadedPageAction | ClearPageAction;

export const loadPage = (page: number, options: GetListParameters, clearList?: boolean) => f(async (dispatch: Dispatch) => {
    dispatch({ type: LOADING, page, clearList });
    const value = await VoIP.getList(page, options);
    dispatch({
        page,
        pages: value.pages,
        type: LOADED_PAGE,
        values: value.results,
    });
});

export const clearLoading = () => ({
    type: CLEAR_LOADING,
});

export const clearCallList = () => ({
    type: CLEAR_PAGE
});
