import { Actions, CLEAR_LOADING, CLEAR_PAGE, LOADED_PAGE, LOADING } from 'src/redux/voip/actions';
import { VoIPState } from 'src/redux/voip/state';

export default (state: VoIPState, action: Actions): VoIPState => {
    switch(action.type) {
        case LOADING: {
            return {
                ...state,
                list: action.clearList ? [] : state.list,
                loading: true,
                page: action.page,
            };
        }

        case CLEAR_LOADING: {
            return {
                ...state,
                loading: false,
            };
        }

        case LOADED_PAGE: {
            const list = [ ...state.list ];
            list[action.page] = action.values;
            return {
                ...state,
                list,
                loading: false,
                totalPages: action.pages,
            };
        }

        case CLEAR_PAGE: {
            return {
                list: [],
                loading: false,
            };
        }

        default:
            return state || {
                list: [],
                loading: false,
            };
    }
};