import { combineReducers } from 'redux';

import { reducers as commonReducers, State as CommonState } from 'src/common/redux';
import voip from 'src/redux/voip/reducer';
import { VoIPState } from 'src/redux/voip/state';

export const reducers = combineReducers<State>({
    ...commonReducers,
    voip,
});

// tslint:disable-next-line:no-empty-interface
export interface State extends CommonState {
    voip: VoIPState;
}
