import React from 'react';
import { Redirect } from 'react-router';
import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

export const moduleName = 'voip';

const UserListHistoryPage = asyncComponent(() => import('./containers/user-list-history-page'));
const AdminListHistoryPage = asyncComponent(() => import('./containers/admin-list-history-page'));

export const routes: Array<Route<any, any>> = [
    route('/', 'home', () => <Redirect to="/1" />, 'home', { exact: true }),
    route('/:from(fd:\\d+/)?:to(td:\\d+/)?:pn(pn:\\+?\\d+/)?:page(\\d+)', 'home', UserListHistoryPage, 'largeHome', { hide: true }),
    route('/admin', 'query', () => <Redirect to="/admin/1" />, 'query', { exact: true }),
    route('/admin/:from(fd:\\d+/)?:to(td:\\d+/)?:pn(pn:\\+?\\d+/)?:apt(apt:.+/)?:page(\\d+)', 'query', AdminListHistoryPage, 'largeQuery', { hide: true }),
];