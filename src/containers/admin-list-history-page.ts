import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import asyncComponent from 'src/common/components/async-component';
import { clearError } from 'src/common/redux/errors/actions';
import { ListHistoryPage } from 'src/components/pages/list-history-page';
import {
    ListHistoryPageDispatchToProps,
    ListHistoryPageStateToProps
} from 'src/components/pages/list-history-page.interfaces';
import { State } from 'src/redux';
import { clearCallList, clearLoading, loadPage, } from 'src/redux/voip/actions';

const AdminFilter = asyncComponent(() => import('src/components/admin-filter'));

const mapStateToProps = ({ errors, voip }: State): ListHistoryPageStateToProps => ({
    filter: AdminFilter,
    list: voip.page !== undefined ? voip.list[voip.page] : undefined,
    loading: voip.loading,
    serverError: errors.serverUnavailable,
    title_i18_key: 'admin_title',
    totalPages: voip.totalPages,
});

const mapDispatchToProps = (dispatch: any): ListHistoryPageDispatchToProps => ({
    clearError: (what) => dispatch(clearError(what)),
    loadPage: (a, b, c) => dispatch(loadPage(a, { ...b, get_all_items: true }, c)),
    unload: () => dispatch(clearCallList()),
    unsetLoading: () => dispatch(clearLoading()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListHistoryPage));