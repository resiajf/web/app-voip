import { AsYouType, isValidNumber } from 'libphonenumber-js';
import React from 'react';

import { Omit } from 'src/common/lib/meta';

interface PhoneInputState {
    value: string;
    isValid: boolean;
}

type PhoneInputProps = Omit<Omit<Omit<React.InputHTMLAttributes<HTMLInputElement>, 'value'>, 'onChange'>, 'type'> & {
    value?: string,
    onChange?: (strong: string, isValid: boolean) => void
};

export class PhoneInput extends React.PureComponent<PhoneInputProps, PhoneInputState> {

    private engine = new AsYouType('ES');

    constructor(props: PhoneInputProps) {
        super(props);

        this.state = {
            isValid: true,
            value: '',
        };

        this.onChange = this.onChange.bind(this);
    }

    public componentDidMount() {
        if(this.props.value !== undefined && this.props.value !== this.state.value) {
            this.makePhoneChange(this.props.value || '');
        }
    }

    public componentDidUpdate(prevProps: Readonly<PhoneInputProps>) {
        if(this.props.value !== undefined && this.props.value !== this.state.value) {
            this.makePhoneChange(this.props.value || '');
        }

        if(!prevProps.disabled && this.props.disabled) {
            this.setState({ isValid: true });
        }
    }

    public render() {
        const { className, onChange, value, ...props } = this.props;

        return (
            <input className={ `form-control ${this.state.isValid ? '' : 'is-invalid'} ${className}` }
                   type="tel"
                   value={ this.state.value }
                   onChange={ this.onChange }
                   {...props} />
        );
    }

    private onChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.makePhoneChange(e.target.value, () => {
            if(this.props.onChange) {
                this.props.onChange(this.state.value, this.state.isValid);
            }
        });
    }

    private makePhoneChange(phone: string, cbk?: () => void) {
        this.engine.reset();
        const value = this.engine.input(phone);
        this.setState({
            isValid: isValidNumber(value, 'ES') || (value.length > 0 && value.length < 3 && Number(value) < 100 && value[0] !== '0'),
            value,
        }, cbk);
    }

}