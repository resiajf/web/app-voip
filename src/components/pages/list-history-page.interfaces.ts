import React from 'react';
import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import { CallRecord, ServerUnavailableError } from 'src/common/lib/api';
import { GetListParameters } from 'src/common/lib/api/voip/methods';
import { ErrorsState } from 'src/common/redux/errors/state';

type FilterComponent<T> = T extends React.Component<{ onFilterApply: (p: GetListParameters) => void, opts: GetListParameters }>;

export interface ListHistoryPageStateToProps {
    loading: boolean;
    totalPages?: number;
    list?: CallRecord[];
    apartment?: number;
    serverError: ServerUnavailableError | null;
    filter: FilterComponent<any>; //Is not a good idea to put this here, but :)
    title_i18_key: string; //Same as above :)
}

export interface ListHistoryPageDispatchToProps {
    clearError: (what: keyof ErrorsState) => void;
    loadPage: (page: number, options: GetListParameters, clearList?: boolean) => void;
    unload: () => void;
    unsetLoading: () => void;
}

interface ListHistoryPageRouteProps {
    page?: string;
    from?: string; // 'fd:NUMBER/'
    to?: string;   // 'td:NUMBER/'
    apt?: string;  // 'apt:STRING/'
    pn?: string;   // 'pn:STRING/'
}

export type ListHistoryPageProps = ListHistoryPageStateToProps & ListHistoryPageDispatchToProps &
    InjectedTranslateProps & RouteComponentProps<ListHistoryPageRouteProps>;