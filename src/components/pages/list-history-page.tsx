import CallMadeIcon from '@material-ui/icons/CallMade';
import CallMissedIcon from '@material-ui/icons/CallMissed';
import CallMissedOutgoingIcon from '@material-ui/icons/CallMissedOutgoing';
import CallReceivedIcon from '@material-ui/icons/CallReceived';
import moment from 'moment';
import React from 'react';
import { Trans, translate } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';
import { Collapse } from 'src/common/components/bootstrap/collapse';
import Pagination from 'src/common/components/pagination';
import { CallRecord } from 'src/common/lib/api';
import { GetListParameters } from 'src/common/lib/api/voip/methods';
import { ListHistoryPageProps } from 'src/components/pages/list-history-page.interfaces';

interface ListHistoryPageState {
    showFilter: boolean;
}

class ListHistoryPageClass extends React.Component<ListHistoryPageProps, ListHistoryPageState> {

    private static prettyDuration(duration: number) {
        const seconds = duration % 60;
        const minutes = Math.trunc(duration / 60 % 60);
        const hours = Math.trunc(duration / 3600);

        return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    }

    private static transform(item: CallRecord) {
        if(item.from_extension.length === 1 || (item.from_extension.length === 2 && Number(item.from_extension) < 100)) {
            return {
                apt: item.from_extension,
                call_length: item.call_length,
                datetime: item.datetime,
                missed: item.call_length === 0,
                phone: item.to_extension,
                received: false,
            };
        }
        if(item.to_extension.length === 1 || (item.to_extension.length === 2 && Number(item.to_extension) < 100)) {
            return {
                apt: item.to_extension,
                call_length: item.call_length,
                datetime: item.datetime,
                missed: item.call_length === 0,
                phone: item.from_extension,
                received: true,
            };
        }
        throw new Error('WTF!?'); //This is an assert: this should not be
    }

    private static getIcon(item: any) {
        if(item.missed) {
            if(item.received) {
                return <CallMissedIcon />;
            } else {
                return <CallMissedOutgoingIcon />;
            }
        } else {
            if(item.received) {
                return <CallReceivedIcon />;
            } else {
                return <CallMadeIcon />;
            }
        }
    }

    constructor(props: ListHistoryPageProps) {
        super(props);

        this.state = {
            showFilter: false,
        };

        this.toggleShowFilter = this.toggleShowFilter.bind(this);
        this.onFilterChange = this.onFilterChange.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
    }

    public componentDidMount() {
        this.props.loadPage(this.page, {
            extension_f1: this.localExtensionParameter,
            extension_f2: this.phoneNumberParameter,
            from_date: this.fromDateParameter ? this.fromDateParameter.toDate() : undefined,
            to_date: this.toDateParameter ? this.toDateParameter.add(1, 'day').toDate() : undefined,
        });
    }

    public componentDidUpdate(prevProps: Readonly<ListHistoryPageProps>) {
        const p1 = prevProps.match.params;
        const p2 = this.props.match.params;
        if(p1.page !== p2.page || p1.to !== p2.to || p1.from !== p2.from || p1.pn !== p2.pn || p1.apt !== p2.apt) {
            this.props.loadPage(this.page, {
                extension_f1: this.localExtensionParameter,
                extension_f2: this.phoneNumberParameter,
                from_date: this.fromDateParameter ? this.fromDateParameter.toDate() : undefined,
                to_date: this.toDateParameter ? this.toDateParameter.add(1, 'day').toDate() : undefined,
            });
        }

        if(this.props.serverError && !prevProps.serverError) {
            this.props.clearError('serverUnavailable');
        }
    }

    public componentWillUnmount() {
        this.props.unload();
    }

    public render() {
        const { t, totalPages, filter: Filter } = this.props;
        const { showFilter } = this.state;

        return (
            <div>
                <h1 className="display-4">
                    <Trans i18nKey={ this.props.title_i18_key }>
                        {{ apt: this.props.apartment }}
                    </Trans>
                </h1>
                <div className="mb-4">
                    <div className="col-12 text-right">
                        <Button type="primary" outline={ true } size="sm" onClick={ this.toggleShowFilter }>
                            { showFilter ? t('hide_filter') : t('show_filter') }
                        </Button>
                    </div>
                    <Collapse show={ showFilter } className="col-12">
                        <Filter onFilterApply={ this.onFilterChange } opts={{
                            extension_f1: this.localExtensionParameter,
                            extension_f2: this.phoneNumberParameter,
                            from_date: this.fromDateParameter ? this.fromDateParameter.toDate() : undefined,
                            to_date: this.toDateParameter ? this.toDateParameter.toDate() : undefined,
                        }} />
                    </Collapse>
                </div>

                <div className="table-responsive-lg">
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col" style={{ minWidth:  60 }}>{ t('table.apt') }</th>
                            <th scope="col" style={{ minWidth: 160 }}>{ t('table.phone') }</th>
                            <th scope="col" style={{ minWidth: 120 }}>{ t('table.duration') }</th>
                            <th scope="col" style={{ minWidth: 220 }}>{ t('table.when') }</th>
                        </tr>
                        </thead>
                        <tbody>
                        { this.props.list ? this.props.list.map(item => ListHistoryPageClass.transform(item)).map((item, i) => (
                            <tr key={ i }>
                                <td>{ item.apt }</td>
                                <td>{ ListHistoryPageClass.getIcon(item) }{ item.phone }</td>
                                <td>{ ListHistoryPageClass.prettyDuration(item.call_length) }</td>
                                <td>{ moment.utc(item.datetime).format('lll') }</td>
                            </tr>
                        )) : <tr><td colSpan={ 4 } className="text-center">{ t('table.loading') }</td></tr> }
                        </tbody>
                    </table>
                </div>

                <Pagination page={ this.page - 1 } pages={ totalPages || 1 } onChange={ this.onPageChange } />
            </div>
        );
    }

    private get fromDateParameter() {
        const { from } = this.props.match.params;
        if(from) {
            try {
                return moment.utc(Number(from.substring(3, from.length - 1)));
            } catch(e) {
                return undefined;
            }
        }
        return undefined;
    }

    private get toDateParameter() {
        const { to } = this.props.match.params;
        if(to) {
            try {
                return moment.utc(Number(to.substring(3, to.length - 1)));
            } catch(e) {
                return undefined;
            }
        }
        return undefined;
    }

    private get localExtensionParameter() {
        const { apt } = this.props.match.params;
        if(apt) {
            return Number(apt.substring(4, apt.length - 1));
        }
        return undefined;
    }

    private get phoneNumberParameter() {
        const { pn } = this.props.match.params;
        if(pn) {
            return pn.substring(3, pn.length - 1);
        }
        return undefined;
    }

    private get page() {
        return Number(this.props.match.params.page || 1);
    }

    private generateUrl(page: number) {
        const { from, to, pn, apt } = this.props.match.params;
        if(this.props.location.pathname.startsWith('/admin')) {
            return `/admin/${from || ''}${to || ''}${pn || ''}${apt || ''}${page}`;
        } else {
            return `/${from || ''}${to || ''}${pn || ''}${apt || ''}${page}`;
        }
    }

    private onPageChange(page: number) {
        this.props.history.push(this.generateUrl(page + 1));
    }

    private toggleShowFilter(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.setState({
            showFilter: !this.state.showFilter,
        });
    }

    private onFilterChange(opts: GetListParameters) {
        let from = '';
        let to   = '';
        let pn   = '';
        let apt  = '';
        if(opts.from_date) {
            from = `fd:${+opts.from_date}/`;
        }
        if(opts.to_date) {
            to = `td:${+opts.to_date}/`;
        }
        if(opts.extension_f1) {
            apt = `apt:${opts.extension_f1}/`;
        }
        if(opts.extension_f2) {
            pn = `pn:${opts.extension_f2}/`;
        }
        if(this.props.location.pathname.startsWith('/admin')) {
            this.props.history.push(`/admin/${from}${to}${pn}${apt}1`);
        } else {
            this.props.history.push(`/${from}${to}${pn}${apt}1`);
        }
    }

}

export const ListHistoryPage = translate()(ListHistoryPageClass);
