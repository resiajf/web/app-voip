import { formatNumber, parseNumber } from 'libphonenumber-js';
import moment from 'moment';
import React from 'react';
import DateTimeInput from 'react-datetime';
import { InjectedTranslateProps, translate } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';
import { GetListParameters } from 'src/common/lib/api/voip/methods';
import { PhoneInput } from 'src/components/phone-input';

import 'react-datetime/css/react-datetime.css';

export interface UserFilterProps extends InjectedTranslateProps {
    onFilterApply: (p: GetListParameters) => void;
    opts: GetListParameters;
}

export interface UserFilterState {
    dateFilter: boolean;
    fromDate?: moment.Moment;
    toDate: moment.Moment;
    extensionFilter: boolean;
    externalExtension?: string;
    externalPhone?: string;
    isExternalPhoneValid: boolean;
}

const nDistinct = (a: number, b: number) => (isNaN(a) && !isNaN(b)) || (!isNaN(a) && isNaN(b)) || (!isNaN(a) && !isNaN(b) && a !== b);

export class UserFilter<
    P extends UserFilterProps = UserFilterProps,
    S extends UserFilterState = UserFilterState
> extends React.Component<P, S> {

    private static untilNowLimit(currentDate: moment.Moment): boolean {
        return currentDate.isBefore(moment.now());
    }

    private static untilTime(whatTime?: moment.Moment) {
        return (currentDate: moment.Moment) => currentDate.isBefore(moment(whatTime).subtract(1, 'day'));
    }

    constructor(props: P) {
        super(props);

        //TODO cast as workaround for https://github.com/Microsoft/TypeScript/issues/10727
        this.state = {
            dateFilter: false,
            extensionFilter: false,
            externalExtension: undefined,
            externalPhone: undefined,
            fromDate: undefined,
            isExternalPhoneValid: true,
            toDate: moment(),
        } as any;

        this.onExternalExtensionChange = this.onExternalExtensionChange.bind(this);
        this.toggleExtensionFilter = this.toggleExtensionFilter.bind(this);
        this.onExternalPhoneChange = this.onExternalPhoneChange.bind(this);
        this.toggleDateFilter = this.toggleDateFilter.bind(this);
        this.onFromDateChange = this.onFromDateChange.bind(this);
        this.cleanFiltering = this.cleanFiltering.bind(this);
        this.onToDateChange = this.onToDateChange.bind(this);
        this.doFiltering = this.doFiltering.bind(this);
    }

    public componentDidMount() {
        //TODO cast as workaround for https://github.com/Microsoft/TypeScript/issues/10727
        const { children, opts, ...props } = this.props as any; //Remove children and opts properties from props
        this.componentDidUpdate({ ...props, opts: {} });
    }

    public componentDidUpdate(prevProps: Readonly<UserFilterProps>) {
        let updated = false;
        let { dateFilter, extensionFilter, fromDate, toDate, externalPhone, externalExtension } = this.state;
        const { opts } = this.props;

        if(nDistinct(Number(prevProps.opts.from_date), Number(opts.from_date))) {
            if(!fromDate && opts.from_date) {
                fromDate = moment.utc(opts.from_date);
                updated = true;
                dateFilter = true;
            } else if(fromDate && !opts.from_date) {
                fromDate = undefined;
                updated = true;
                dateFilter = true;
            } else if(fromDate && !fromDate.isSame(moment.utc(opts.from_date))) {
                fromDate = moment.utc(opts.from_date);
                updated = true;
                dateFilter = true;
            }
        }

        if(nDistinct(Number(prevProps.opts.to_date), Number(opts.to_date))) {
            if(!toDate && opts.to_date) {
                toDate = moment.utc(opts.to_date);
                updated = true;
                dateFilter = true;
            } else if(toDate && !opts.to_date) {
                toDate = moment();
                updated = true;
                dateFilter = true;
            } else if(toDate && !toDate.isSame(moment.utc(opts.to_date))) {
                toDate = moment.utc(opts.to_date);
                updated = true;
                dateFilter = true;
            }
        }

        if(opts.extension_f2 !== prevProps.opts.extension_f2) {
            if(!externalPhone && opts.extension_f2) {
                const p = parseNumber(opts.extension_f2, { defaultCountry: 'ES', extended: true });
                if(p.valid) {
                    if(p.countryCallingCode && p.countryCallingCode !== '34') {
                        externalPhone = `${p.countryCallingCode ? '+' + p.countryCallingCode + ' ' : ''}${p.phone}`;
                    } else {
                        externalPhone = p.phone.toString();
                    }
                    externalExtension = p.ext ? p.ext.toString() : p.ext;
                    updated = true;
                    extensionFilter = true;
                }
            }
        }

        if(updated) {
            this.setState({
                dateFilter,
                extensionFilter,
                externalExtension,
                externalPhone,
                fromDate,
                isExternalPhoneValid: true,
                toDate,
            });
        }
    }

    public render() {
        const { dateFilter, extensionFilter, externalPhone, isExternalPhoneValid } = this.state;
        const { t } = this.props;

        const cannotDoFiltering = (extensionFilter && (!externalPhone || !isExternalPhoneValid)) ||
            (!dateFilter && !extensionFilter);

        return (
            <form>
                { this.filterByDateSection }
                { this.filterByExternalPhoneSection }

                <Button type="primary"
                        outline={ true }
                        className="mr-1"
                        onClick={ this.doFiltering }
                        disabled={ cannotDoFiltering } >
                    { t('filter.do_filtering') }
                </Button>
                <Button type="secondary" outline={ true } onClick={ this.cleanFiltering }>
                    { t('filter.clear') }
                </Button>
            </form>
        );
    }

    protected stateToOptions() {
        const opts: GetListParameters = {};
        if(this.state.dateFilter) {
            if(this.state.fromDate) {
                opts.from_date = this.state.fromDate.utc().toDate();
            }
            opts.to_date = this.state.toDate!.utc().toDate();
        }
        if(this.state.extensionFilter) {
            if(this.state.externalPhone!.length >= 3) {
                try {
                    //TODO check if this format is valid
                    const phone = formatNumber(parseNumber(this.state.externalPhone!, 'ES'), 'E.164');
                    opts.extension_f2 = `${phone}${this.state.externalExtension || ''}`;
                } catch(e) {
                    //nothing to do
                }
            } else {
                const num = Number(this.state.externalPhone!);
                if(num >= 0 && num < 100) {
                    opts.extension_f2 = `${num}${this.state.externalExtension || ''}`;
                }
            }
        }
        return opts;
    }

    protected get filterByDateSection() {
        const { dateFilter, fromDate, toDate } = this.state;
        const { t } = this.props;

        return (
            <div className="row">
                <div className="col-12">
                    <div className="form-check">
                        <input type="checkbox"
                               className="form-check-input"
                               id="date-filter"
                               checked={ dateFilter }
                               onChange={ this.toggleDateFilter } />
                        <label htmlFor="date-filter" className="form-check-label">{ t('filter.by_date') }</label>
                    </div>
                </div>
                <div className="form-group col-12 col-sm-6">
                    <label htmlFor="from-date">{ t('filter.from_date') }</label>
                    <DateTimeInput value={ fromDate }
                                   isValidDate={ UserFilter.untilTime(toDate) }
                                   onChange={ this.onFromDateChange }
                                   inputProps={{ id: 'from-date', disabled: !dateFilter, placeholder: t('filter.from_date_ph') }} />
                    <small className="form-text text-muted">{ t('filter.from_date_help') }</small>
                </div>
                <div className="form-group col-12 col-sm-6">
                    <label htmlFor="to-date">{ t('filter.to_date') }</label>
                    <DateTimeInput value={ toDate }
                                   isValidDate={ UserFilter.untilNowLimit }
                                   onChange={ this.onToDateChange }
                                   inputProps={{ id: 'to-date', disabled: !dateFilter }} />
                    <small className="form-text text-muted">{ t('filter.to_date_help') }</small>
                </div>
            </div>
        );
    }

    protected get filterByExternalPhoneSection() {
        const { extensionFilter, externalPhone, externalExtension, isExternalPhoneValid } = this.state;
        const { t } = this.props;

        return (
            <div className="row">
                <div className="col-12">
                    <div className="form-check">
                        <input type="checkbox"
                               className="form-check-input"
                               id="ext-filter"
                               onChange={ this.toggleExtensionFilter }
                               checked={ extensionFilter } />
                        <label htmlFor="ext-filter" className="form-check-label">{ t('filter.by_extension') }</label>
                    </div>
                </div>
                <div className="form-group col-12 col-sm-12">
                    <label htmlFor="phone">{ t('filter.external_phone') }</label>
                    <PhoneInput id="phone"
                                value={ externalPhone || '' }
                                disabled={ !extensionFilter }
                                onChange={ this.onExternalPhoneChange } />
                    <small className="form-text text-muted">{ t('filter.external_phone_help') }</small>
                </div>
                <div className="form-group col-12 col-sm-4 d-none">
                    <label htmlFor="extension">{ t('filter.external_extension') }</label>
                    <input className="form-control"
                           id="extension"
                           value={ externalExtension || '' }
                           onChange={ this.onExternalExtensionChange }
                           maxLength={ 15 /* TODO I don't know exactly if there's a limit in extensions, but 15 is pretty high */ }
                           disabled={ !extensionFilter || !externalPhone || !isExternalPhoneValid || externalPhone.length < 3 } />
                    <small className="form-text text-muted">{ t('filter.external_extension_help') }</small>
                </div>
            </div>
        );
    }

    protected cleanFiltering(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.setState({
            dateFilter: false,
            extensionFilter: false,
            externalExtension: '',
            externalPhone: '',
            fromDate: undefined,
            toDate: moment(),
        }, () => this.props.onFilterApply && this.props.onFilterApply({}));
    }

    protected doFiltering(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        if(this.props.onFilterApply) {
            this.props.onFilterApply(this.stateToOptions());
        }
    }

    private toggleDateFilter() {
        this.setState({
            dateFilter: !this.state.dateFilter,
            toDate: !this.state.extensionFilter ? this.state.toDate || moment() : this.state.toDate,
        });
    }

    private toggleExtensionFilter() {
        this.setState({
            extensionFilter: !this.state.extensionFilter,
        });
    }

    private onFromDateChange(date: moment.Moment | string) {
        this.setState({
            fromDate: typeof date === 'string' ? moment() : date,
        });
    }

    private onToDateChange(date: moment.Moment) {
        let { fromDate } = this.state;
        const dayLess = moment(date).subtract(1, 'day');

        if(fromDate && !fromDate.isBefore(dayLess)) {
            fromDate = dayLess;
        }

        this.setState({
            fromDate,
            toDate: date,
        });
    }

    private onExternalPhoneChange(externalPhone: string, isExternalPhoneValid: boolean) {
        this.setState({
            externalPhone,
            isExternalPhoneValid,
        });
    }

    private onExternalExtensionChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            externalExtension: e.target.value.trim().replace(/[^0-9]+/g, '')
        });
    }

}

export default translate()(UserFilter);