import React from 'react';
import { translate } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';
import { GetListParameters } from 'src/common/lib/api/voip/methods';
import { UserFilter, UserFilterProps, UserFilterState } from 'src/components/user-filter';

export interface AdminFilterState extends UserFilterState {
    internalFilter: boolean;
    internalPhone: number;
}

export class AdminFilter extends UserFilter<UserFilterProps, AdminFilterState> {

    constructor(props: UserFilterProps) {
        super(props);

        //Partially initialized in UserFilter, we could do this :)
        // @ts-ignore
        this.state = {
            ...this.state,
            internalFilter: false,
            internalPhone: 1,
        };

        this.onInternalPhoneChanged = this.onInternalPhoneChanged.bind(this);
        this.toggleInternalFilter = this.toggleInternalFilter.bind(this);
    }

    public render() {
        const { dateFilter, extensionFilter, externalPhone, isExternalPhoneValid, internalFilter } = this.state;
        const { t } = this.props;

        const cannotDoFiltering = (extensionFilter && (!externalPhone || !isExternalPhoneValid)) ||
            (!dateFilter && !extensionFilter && !internalFilter);

        return (
            <form>
                { this.filterByDateSection }
                { this.filterByInternalPhoneSection }
                { this.filterByExternalPhoneSection }

                <Button type="primary"
                        outline={ true }
                        className="mr-1"
                        onClick={ this.doFiltering }
                        disabled={ cannotDoFiltering } >
                    { t('filter.do_filtering') }
                </Button>
                <Button type="secondary" outline={ true } onClick={ this.cleanFiltering }>
                    { t('filter.clear') }
                </Button>
            </form>
        );
    }

    protected get filterByInternalPhoneSection() {
        const { internalFilter, internalPhone } = this.state;
        const { t } = this.props;

        return (
            <div className="row">
                <div className="col-12">
                    <div className="form-check">
                        <input type="checkbox"
                               className="form-check-input"
                               id="internal-filter"
                               checked={ internalFilter }
                               onChange={ this.toggleInternalFilter } />
                        <label htmlFor="internal-filter" className="form-check-label">{ t('filter.by_internal') }</label>
                    </div>
                </div>
                <div className="col-12">
                    <div className="form-group">
                        <label htmlFor="internal-phone">{ t('filter.internal_phone') }</label>
                        <input type="number"
                               className="form-control"
                               value={ internalPhone }
                               onChange={ this.onInternalPhoneChanged }
                               min={ 0 }
                               max={ 99 }
                               disabled={ !internalFilter } />
                    <small className="form-text text-muted">{ t('filter.internal_phone_help') }</small>
                    </div>
                </div>
            </div>
        );
    }

    protected stateToOptions(): GetListParameters {
        if(this.state.internalFilter) {
            return {
                ...super.stateToOptions(),
                extension_f1: this.state.internalPhone!,
            };
        } else {
            return super.stateToOptions();
        }
    }

    private toggleInternalFilter() {
        this.setState({
            internalFilter: !this.state.internalFilter,
        });
    }

    private onInternalPhoneChanged(e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            internalPhone: Math.max(0, Math.min(Number(e.target.value), 99)),
        });
    }

}

export default translate()(AdminFilter);
